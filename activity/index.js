//alert("hello world");

//create an object using object literals
let trainer ={};

//properties
trainer.name = 'Ash Ketchump';
trainer.age = 10;
trainer.pokemon = ['pikachu','charizard','squirtel', 'bulbasaur' ]
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
}

//method 
trainer.talk =function(){
	console.log("pickachu i choose you")
}
trainer.talk()

//check ig all props are added
console.log(trainer)

//access object properties using dot notation
console.log("result of dot notation")
console.log(trainer.name)

//access object properties using square brackets
console.log("result of bracket notation")
console.log(trainer['pokemon'])

//trainer talk method
console.log("reult of talk")
trainer.talk()

//Create a constructor function for creating a pokemon
function Pokemon(name, level){

	//properties 
	this.name =name;
	this.level =level;
	this.health =2*level;
	this.attack =level;

	//methods
	this.tackle = function(target){
		console.log(this.name +" tackled "+target.name);
		
		target.health -= this.attack;

	console.log(target.name + "is not reduced "+ target.health)


	if (target.health <=0){
		target.faint()
	}
		};
	this.faint = function(){
		console.log(this.name +" fainted")
	}
}


//new pokemon
let picakhu =new Pokemon("pickachu", 12);
console.log(picakhu);

// 
let raichu = new Pokemon("raichu", 15);
console.log(raichu);

//
let missingno = new Pokemon("missingno", 18);
console.log(missingno);

//invoke  the takle method

raichu.tackle(picakhu);
console.log(picakhu);

missingno.tackle(picakhu);
console.log(picakhu);






