//alert("hello world");

//objects 
/*
	an object is a data type that is used to represent real world objects

	it is a collection of related data and or functionalities

	information stored in objects are represented in "key:value" pair

	different data types may be stores in an object


*/

//creating object
/*
	2 way to create object

	1. object initializers/literal Notation

	2.constructor function
*/


//creating object through object initializers / literal notation

/*
	this create an object and also initializes / assign ots valuses upon creation

	it already has its key values such as name, color, weight, etc

	syntax:
	let objectName ={
		keyA: valueA,
		keyB: valueB
	};
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("result from creationg object using initializers notation");

console.log(cellphone);
console.log(typeof cellphone);

// constructor function
/*
	creates a reusable functions to create several objects that have the same data structure 

	useful for creating multiple/instance copies of an object 

	an "instance" is concrete occurence of any object which emphazies on

	syntax:
	function Objectname(name, keyB){
	this.keyA = name,
	this.keyB = keyB
	};
*/



function Laptop(name, manufactureDate){
	this.name =name;
	this.manufactureDate =manufactureDate;

}

function Student(firstName, lastName, yearOld, city){
	this.name =firstName + " "+ lastName;
	this.age =yearOld;
	this.residence =city;
}

/*
	the this keyword allow us to assign a new object properties by associating the, with valuses received from, a constructor function
*/
//instance of laptop object
console.log("this is a unique instance of the laptop object using object conmstructor")
let laptop_mica = new Laptop('lenovo', 2008)
console.log(laptop_mica);

let laptop_eric = new Laptop('HP', 1990)
console.log(laptop_eric);

console.log("this is a unique instance of the Student object")
let elaine =new Student('Elaine', 'sj', 12, 'mars');
console.log(elaine);

let jake =new Student('jake', 'lexter', 10, 'sun');
console.log(jake);

/*
 	the "new" keyword is an instance of new object 
	
*/
console.log("result of creating object without new keyword (undefined)")
let oldLaptop = Laptop('portal R3E', 1980);
console.log(oldLaptop);


//access object properties
// ther are 2 way to access

//using the dot notation
console.log(laptop_mica.name);
console.log(elaine.name);


//bracket notation 
console.log("using bracket")
//laptop name
console.log(laptop_mica['name']);

console.log("another way")
let studentLaptop = laptop_mica.name;
console.log(studentLaptop);


// initialize, add, delete. reassign object

/*
	initialize any otyher variable in javascript ahve their own properties initialized after the object was crated

	this is useful for times when an object properties undertermined art the time of beginning 

*/

//empty object
console.log("empty car object")
let car ={};
console.log(car);
console.log("");

console.log("result from adding properties using dot notation");
car.name = "Honda Civic";
console.log(car);

//case sensitive
car['manufacture date'] =2019;
console.log(car['manufacture date']);
console.log("result from adding properties using bracket notation");
console.log(car);
// we used the bracket notation to access property names with spaces
console.log("");

//delete object properties
delete car['manufacture date'];
//delete car.name; //goods din to
console.log("result from delete properties");
console.log(car);
console.log("");

//reassign object properties 
car.name = 'dodge charger R/T'
console.log("result from reassign properties");
console.log(car);
console.log("");


//object methods
/*
	a method is a function 

	a method is a function attached to an object as an property 

	a method is a useful for creating object specific properties

	similar to function of real world objects method are defined based on what an object is a capable of doing


*/



let person = {
	name: 'john',
	talk: function(){
		console.log("hello " + this.name)
	}
};

console.log(person);
console.log("result from object methods");
person.talk();



// 
person.walk = function(){
	console.log(this.name + ' walked 25 step forward');
}

person.walk();

//methods are usefull for creating reusable functions that performs tasks related to object
let friend = {
	firstName: 'Ar Jay',
	lastName: 'Dala',
	address: {
		city:'Etibak',
		country: 'New York'
	},
	email: ['arjay@myFriendster.com', 'arjay@mySpace.com'],
	introduce: function(classmate){
		console.log("hello my name is "+this.firstName + " "+ this.lastName +" nice to meet you "+ classmate)
	}
}
friend.introduce('eric');
friend.introduce('mica');
friend.introduce('kyle');

//

let myPokemon = {
	name: 'pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(targetPokemon){
		console.log(this.name +" pokemon tackled "+ targetPokemon)
		console.log(targetPokemon + "'s health is redunced to "+ targetPokemon+ " health")
	},
	faint: function(){
		console.log("pokemon fainted")
	}
}

console.log(myPokemon);
myPokemon.tackle('charmander');
myPokemon.tackle('arceous');
console.log("");

//using constructor function
function Pokemon(name, level){
	this.name = name,
	this.level=level,
	this.health = 2 *level;
	this.attack = level;
	this.tackle =function(target){
		console.log(this.name + ' tackled '+ target.name)
		console.log(target.name + ' s health reduce to '+ target.health)
	};
	this.faint = function (){
		console.log(this.name + 'fainted')
	}
}


let picakhu = new Pokemon('pikachu', 16);
let ratata = new Pokemon('ratata', 8);

picakhu.tackle(ratata)









